# DisposableIncomes

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://greimel.gitlab.io/DisposableIncomes.jl/dev)
[![Build Status](https://gitlab.com/greimel/DisposableIncomes.jl/badges/master/build.svg)](https://gitlab.com/greimel/DisposableIncomes.jl/pipelines)
[![Coverage](https://gitlab.com/greimel/DisposableIncomes.jl/badges/master/coverage.svg)](https://gitlab.com/greimel/DisposableIncomes.jl/commits/master)
