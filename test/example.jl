# Specify life-cycle profile

# Lifecycle component (Kaplan & Violante 2010)
# fourth-order polynomial regression of yearly income on age from the PSID

#using Revise #src
using DisposableIncomes
using Test
using LinearAlgebra
using GLM, DataFrames, LinearAlgebra
using Parameters
using QuantEconX: tauchen
using MarkovChainsX

@testset "fourth-order polynomial" begin
  χ = DisposableIncomes.χ
  age = 1:35
  df = DataFrame(χ = χ, age = age)

  out = lm(@formula(χ ~ age + age^2 + age^3 + age^4), df)
  
  # use fitted values from GL(M
  @test predict(out) ≈ χ
  # compute fitted values by hand
  χ_fit(t) = dot(coef(out), [1, t, t^2, t^3, t^4])
  @test χ_fit.(age) ≈ χ
end

# Specify shock process
n_y = 13
y = tauchen(n_y, 0.91, 0.21)

y0_mc = MarkovChain(y.p, y.state_values, :y)

# add unemployment
πᵤ, πₑ = 0.2, 0.7
y1_mc = add_unemployment(y0_mc, πᵤ, πₑ)

# first block of matrix
@test all(y1_mc.p[1:n_y,1:n_y] .≈ y0_mc.p .* (1 .- πᵤ))

gross_inc0x = SimpleGrossIncomes(mc_work=y0_mc)
gross_inc1x = SimpleGrossIncomes(mc_work=y1_mc)
gross_inc0 = SimpleLifecycleGrossIncomes(mc_work=y0_mc)
gross_inc1 = SimpleLifecycleGrossIncomes(mc_work=y1_mc)

import DisposableIncomes: has_unemployment, simulate_work, gross_income, potential_income

@test has_unemployment(gross_inc0x) == false
@test has_unemployment(gross_inc1x) == true
@test has_unemployment(gross_inc0) == false
@test has_unemployment(gross_inc1) == true

# XXX simulate!!

sim0x = simulate_work(gross_inc1x, 10, n_t=100, f_income = potential_income)
sim1x = simulate_work(gross_inc1x, 10, n_t=100, f_income = gross_income)
sim0 = simulate_work(gross_inc1, 10, f_income = potential_income)
sim1 = simulate_work(gross_inc1, 10, f_income = gross_income)

@test all(sim0 .> 0)
@test any(sim1 .== 0)

# ---------------------------------------------
# Taxes
# ---------------------------------------------

tax0x = HeathcoteStoreslettenViolante2017(gross_inc0x, 0.2, 10_000, n_t=100)
tax1x = HeathcoteStoreslettenViolante2017(gross_inc1x, 0.2, 10_000, n_t=100)

tax0 = HeathcoteStoreslettenViolante2017(gross_inc0, 0.2, 10_000)
tax1 = HeathcoteStoreslettenViolante2017(gross_inc1, 0.2, 10_000)

tax_det = HeathcoteStoreslettenViolante2017(0.2, 0.15)

@test tax0.λ < tax1.λ

# ---------------------------------------------
# Pensions
# ---------------------------------------------

gs = GuvenenSmith2014(gross_inc0, 10_000)
gs_det = GuvenenSmith2014(0.83, 0.33, 1.19)
@test benefits(1.0, gs_det) ≈ 0.5782600000000001

# ---------------------------------------------
# DisposableIncomes
# ---------------------------------------------

tax_trans = TaxesTransfers(gross_inc1, taxes=tax_det, pensions=gs_det)

DisposableIncomes.retirement_age(tax_trans) == 36

DisposableIncomes.is_working_age(gross_inc0, (ν=0, y=1.5, i_t=34))
DisposableIncomes.is_unemployed(gross_inc1, (ν=1, y=1.5, i_t=34))

# employed
@test disposable_income(gross_inc1, tax_trans, (ν=0, y=1.5, i_t=34)) ≈ 3.7227460102220067
# unemployed
@test disposable_income(gross_inc1, tax_trans, (ν=1, y=1.5, i_t=34)) ≈ 1.4181420322233351
# retired
@test disposable_income(gross_inc0, tax_trans, (ν=1, y=1.5, i_t=70)) ≈ 0.8482331721054773

