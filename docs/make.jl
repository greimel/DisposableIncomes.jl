using Documenter, DisposableIncomes

makedocs(;
    modules=[DisposableIncomes],
    format=Documenter.HTML(),
    pages=[
        "Home" => "index.md",
    ],
    repo="https://gitlab.com/greimel/DisposableIncomes.jl/blob/{commit}{path}#L{line}",
    sitename="DisposableIncomes.jl",
    authors="Fabian Greimel <fabgrei@gmail.com>",
    assets=String[],
)
