struct NoGovernment end

retirement_age(tt::NoGovernment) = Inf

disposable_income(inc, ::NoGovernment, state) = gross_income(inc, state)

struct TaxesTransfers{T1,T2,T3,T4}
  taxes::T1
  ui::T2
  pensions::T3
  retirement_age::T4
end

function TaxesTransfers(inc;
    taxes=HeathcoteStoreslettenViolante2017(inc, 0.2, 10_000),
    ui=has_unemployment(inc) ? ProportionalUI() : NoUI(),
    pensions=GuvenenSmith2014(inc, 10_000),
    retirement_age=length(age_profile(inc)) + 1)

    TaxesTransfers(taxes, ui, pensions, retirement_age)
end

retirement_age(tt::TaxesTransfers) = tt.retirement_age

function disposable_income(inc, tax_trans, state)
  @unpack taxes, pensions, ui = tax_trans
  
  if is_working_age(inc, state)
    y_pot = potential_income(inc, state)
    y_pot_net = y_pot - _taxes(y_pot, taxes)
    
    if is_unemployed(inc, state) ## unemployed
      return ui.b * y_pot
    else
      return y_pot_net
    end
  else # TODO eventually replace t in state with retirement age
    y_pot = potential_income(inc, state)
    y_pot_net = y_pot - _taxes(y_pot, taxes)
    
    return benefits(y_pot_net, pensions)
  end

end

