import StateGrids: product_of_named_grids, transition_matrix, named_grid
import MarkovChainsX: simulate, simulate!, MarkovChain

abstract type GrossLaborIncomes end

# Lifecycle profile from Kaplan & Violante
# "! Kai component of earnings process" in BGLV replication kit
χ = [-0.520437830949535; -0.445723729949535; -0.378341829949534; -0.317627929949534; -0.262959589949535; -0.213756129949534; -0.169478629949534; -0.129629929949534; -0.0937546299495352; -0.0614390899495344; -0.0323114299495341; -0.00604152994953496; 0.0176589700504652; 0.0390366700504658; 0.0582964100504647; 0.0756012700504658; 0.0910725700504656; 0.104789870050465; 0.116790970050464; 0.127071910050466; 0.135586970050465; 0.142248670050465; 0.146927770050466; 0.149453270050464; 0.149612410050466; 0.147150670050465; 0.141771770050466; 0.133137670050466; 0.120868570050465; 0.104542910050465; 0.0836973700504653; 0.0578268700504655; 0.0263845700504652; -0.0112181299495353; -0.0556115899495339]

# -----------------------------------------------------------
# Infinite horizon
# -----------------------------------------------------------
@with_kw struct SimpleGrossIncomes{T1} <: GrossLaborIncomes
  mc_work::T1
end

is_working_age(inc::SimpleGrossIncomes, state) = true

potential_income(inc::SimpleGrossIncomes, state) = exp(state.y)

transition_matrix(inc::SimpleGrossIncomes) = transition_matrix(inc.mc_work)

named_grid(inc::SimpleGrossIncomes) = named_grid(inc.mc_work)

# -----------------------------------------------------------
# Finite horizon
# -----------------------------------------------------------

@with_kw struct SimpleLifecycleGrossIncomes{T1,T2,T3} <: GrossLaborIncomes
  mc_work::T1
  mc_ret::T2 = MarkovChain(Diagonal(ones(length(mc_work.state_values))), mc_work.state_values)
  age_profile::T3 = χ
end

is_working_age(inc::SimpleLifecycleGrossIncomes, state) = state.i_t <= length(age_profile(inc))

potential_income(inc::SimpleLifecycleGrossIncomes, state) = exp(age_effect(inc, state) + state.y)

age_grid(inc::SimpleLifecycleGrossIncomes) = 1:length(inc.age_profile)

age_profile(inc::SimpleLifecycleGrossIncomes) = inc.age_profile

age_effect(inc::SimpleLifecycleGrossIncomes, state) = state.i_t <= lastindex(age_profile(inc)) ? age_profile(inc)[state.i_t] :  inc.age_profile[end] # #should be changed when disposable incomes is fixed # minimum(age_profile(inc))

transition_matrix(inc::SimpleLifecycleGrossIncomes, j) = j <= length(age_profile(inc)) ?  transition_matrix(inc.mc_work) : transition_matrix(inc.mc_ret)

named_grid(inc::SimpleLifecycleGrossIncomes) = named_grid(inc.mc_work)

# -----------------------------------------------------------
# Unemployment
# -----------------------------------------------------------

gross_income(inc, state) = (is_working_age(inc, state) && !is_unemployed(inc, state)) ? potential_income(inc, state) : zero(potential_income(inc, state))

is_unemployed(inc, state) = has_unemployment(inc) && (state.ν == 1)

has_unemployment(inc::GrossLaborIncomes) = :ν in keys(inc.mc_work.state_values[1])

function add_unemployment(mc, πᵤ, πₑ)
  Q = [mc.p * (1 - πᵤ)   I * πᵤ;
       I * πₑ              I * (1-πₑ)]

  ν_grid = [(ν=0,), (ν=1,)]

  grids = product_of_named_grids(mc.state_values, ν_grid)

  MarkovChain(Q, grids)
end
  
# -----------------------------------------------------------
# Simulation
# -----------------------------------------------------------

function simulate_work(inc::GrossLaborIncomes, n_draws=1; n_t=length(age_grid(inc)), f_income=gross_income)
  out = zeros(n_t, n_draws)
  simulate_work!(out, inc; f_income=f_income)
  out
end

function simulate_work!(out, inc::GrossLaborIncomes; f_income)
  n_t, n_draws = size(out)
  
  mc = inc.mc_work
  grid = mc.state_values
  
  inds = zeros(Int, n_t)
  
  for i in 1:n_draws
    simulate_indices!(inds, mc)
    for j in 1:n_t
      θ_j = grid[inds[j]]
      out[j,i] = f_income(inc, (θ_j..., i_t=j))
    end
  end
end



