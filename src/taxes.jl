using Roots

abstract type IncomeTaxes end

@with_kw struct HeathcoteStoreslettenViolante2017{T1,T2} <: IncomeTaxes
  λ::T1 # scale needs to be calibrated to aggregate tax burden
  τ::T2 # progressivity
end

LinearTaxes(τ) = HeathcoteStoreslettenViolante2017(τ, 0.0)
NoTaxes() = HeathcoteStoreslettenViolante2017(0.0, 0.0)

# 0.2
function HeathcoteStoreslettenViolante2017(gross_inc, agg_tax_ratio, N; n_t=length(age_grid(gross_inc)), τ=0.15)
  
  sim = zeros(n_t, N)

  simulate_work!(sim, gross_inc, f_income=gross_income)

  gdp = sum(sim)
  
  tax_burden(λ) = sum(_taxes.(sim, λ, τ))
  
  λ_cali = fzero(λ -> tax_burden(λ) - agg_tax_ratio * gdp, 0, 1)
  
  HeathcoteStoreslettenViolante2017(λ_cali, τ)
end

function _taxes(taxable_y, hsv::HeathcoteStoreslettenViolante2017)
  @unpack λ, τ = hsv
  _taxes(taxable_y, λ, τ)
end

function _taxes(taxable_y, λ, τ)
  λ * taxable_y ^ (1-τ)
end

