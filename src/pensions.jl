## Retirement period (Guvenen & Smith)

using GLM, DataFrames, Statistics, Parameters, MarkovChainsX

abstract type PensionSystem end

struct NoPensionSystem <: PensionSystem end

struct GuvenenSmith2014{T1,T2,T3} <: PensionSystem
  k₀::T1
  k₁::T2
  Y_bar::T3
end

function GuvenenSmith2014(inc, N)
  
  sim = zeros(length(age_grid(inc)), N)

  simulate_work!(sim, inc, f_income=gross_income)

  Yᵢ_bar = dropdims(mean(sim, dims=1), dims=1)
  Y_R   = sim[end,:]

  df = DataFrame(Yᵢ_bar = Yᵢ_bar, Y_R = Y_R)

  reg = lm(@formula(Yᵢ_bar ~ Y_R), df)

  k₀, k₁ = coef(reg)

  Y_bar = mean(Yᵢ_bar) 
  
  GuvenenSmith2014(k₀, k₁, Y_bar)
end

function benefits(Y_R, GS::GuvenenSmith2014) # Y_i = Y(Y_R, Y_bar)
  @unpack k₀, k₁, Y_bar = GS
  
  Y_hat = k₀ + k₁ * Y_R

  Y_tilde = Y_hat / Y_bar 
  
  if Y_tilde <= 0.3
    tmp = 0.9 * Y_tilde
  elseif Y_tilde <= 2
    tmp = 0.27 + 0.32 * (Y_tilde - 0.3)
  elseif Y_tilde <= 4.1
    tmp = 0.81 + 0.15 * (Y_tilde - 2.0)
  else
    tmp = 1.13
  end
  Y_bar * tmp
end