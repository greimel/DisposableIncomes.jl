using Parameters

abstract type UnemploymentInsurance end

@with_kw struct ProportionalUI{T} <: UnemploymentInsurance
  b::T = 0.32
end

struct NoUI <: UnemploymentInsurance end
